variable "environment" {
  type        = string
  description = "The environment name"
}

variable "name" {
  type        = string
  description = "Unique name in the project for the load balancer"
}

variable "bucket_name" {
  type        = string
  description = "Bucket name used for the backend"
}

variable "cdn_domain" {
  type        = string
  description = "domain for the google managed ssl cert"
}

variable "signed_url_cache_max_age_sec" {
  type        = number
  description = "Maximum number of seconds the response to a signed URL request will be considered fresh"
  default     = 604800 // 1 week
}

variable "ssl_policy" {
  type        = string
  description = "Minimum SSL version that needs to be enforced for the environment"
  default     = null
}
