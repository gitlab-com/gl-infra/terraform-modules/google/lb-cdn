data "google_project" "project" {}

resource "random_password" "url_signature" {
  length  = 16
  special = true
}

resource "google_compute_backend_bucket" "default" {
  name        = format("%v-%v", var.environment, var.name)
  bucket_name = var.bucket_name
  enable_cdn  = true
  cdn_policy {
    signed_url_cache_max_age_sec = var.signed_url_cache_max_age_sec
    request_coalescing           = true
  }
}

resource "google_compute_backend_bucket_signed_url_key" "default" {
  name = format("%v-%v", var.environment, var.name)
  # string replaces ensure that the base64 result is URL compliant
  key_value      = replace(replace(base64encode(random_password.url_signature.result), "+", "-"), "/", "_")
  backend_bucket = google_compute_backend_bucket.default.name
}

resource "google_compute_url_map" "default" {
  name            = format("%v-%v", var.environment, var.name)
  default_service = google_compute_backend_bucket.default.self_link
}

resource "google_compute_managed_ssl_certificate" "default" {
  name = format("%v-%v", var.environment, var.name)

  managed {
    domains = [var.cdn_domain]
  }
}

resource "google_compute_target_https_proxy" "default" {
  name             = format("%v-%v", var.environment, var.name)
  url_map          = google_compute_url_map.default.self_link
  ssl_certificates = [google_compute_managed_ssl_certificate.default.self_link]
  ssl_policy       = var.ssl_policy
}

resource "google_compute_global_address" "default" {
  name         = format("%v-%v", var.environment, var.name)
  ip_version   = "IPV4"
  address_type = "EXTERNAL"
}

resource "google_compute_global_forwarding_rule" "https" {
  name       = format("%v-%v-https", var.environment, var.name)
  target     = google_compute_target_https_proxy.default.self_link
  ip_address = google_compute_global_address.default.address
  port_range = "443"
}

resource "google_storage_bucket_iam_member" "cdn" {
  bucket = var.bucket_name
  role   = "roles/storage.objectViewer"
  member = format("serviceAccount:service-%v@cloud-cdn-fill.iam.gserviceaccount.com", data.google_project.project.number)
}
